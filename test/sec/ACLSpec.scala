package util
package sec

import scalaprops._
import prelude._, std._, test._
import Permission._

object ACLSpec extends Scalaprops {

  val basicTest = Property.forAll {

    val acl = ACL[Int]()
      .put(42, Read)
      .put(42, Write)

    acl get 42                           mustEqual Mode(true, true, false).just
    acl get (42, Execute)                mustEqual false
    acl.delete(42, Write).get(42, Write) mustEqual false
  }

} //ACLSpec
