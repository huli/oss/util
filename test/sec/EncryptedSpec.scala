package util
package sec

import scalaprops._
import prelude._, std._, test._
import data._, typ.hlist._, typ.nat._
import std.instances.data._

object EncryptedSpec extends Scalaprops {

  val basicTest = Property.forAll {

    case class C(foo: String, bar: String)

    implicit val DataC: Data[C] = Struct(
      "c",
      "foo" -> empty[String] :+: "bar" -> empty[String] :+: HNil)(
      (c: C) => c.foo :+: c.bar :+: HNil)(
      (c: String:+:String:+:HNil) => C(c.i[_0], c.i[_1]))

    implicit val EncryptC: Encrypt[C] = Encrypt.instance(
      _.copy(foo = ""), _.copy(foo = "foo") )

//======================================================================
    val c1 = C("foo", "bar")
    val c2 = Encrypt[C].encrypt(c1)
    val c3 = Data.toDatum(c2)
    val c4 = Data[Encrypted[C]].fromDatum(c3)
    val c5 = Encrypt[C].decrypt(c4)

    c1 mustEqual c5
  }

} //EncryptedSpec
