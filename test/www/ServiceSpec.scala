package util
package www

import scalaprops._

import prelude._, std._, test._

import std.instances.data._

object ServiceSpec extends Scalaprops {
  object Config extends svc.Env {
    lazy val WWW_DIR = resource("www")
  }

  val format = data.format.Text(data.Charset.Utf8)

  object WWW  extends io.device.Disk("www", Config.WWW_DIR)(format)
  object Test extends WWW.Fragment[String, String]("test")

  val addr = net.netty.Address("localhost", 6789)

  object MyServer extends net.netty.http.Server(
    addr, new Server(Test, "/stripe/index.html"))

  object MyClient extends Client(
    new net.netty.http.Client(addr))

  val endToEndTest = Property.forAll {

    val task = for {
      _    <- MyServer.start & Entry.nop
    //_    <- Task0.sleep(Minutes(1)) & Entry.nop //test manually with browser
      res1 <- MyClient.get("/stripe/index.html")()
      res2 <- MyClient.get("/")()
      res3 <- MyClient.get("/stripe/../../../env/API_KEY_POSTMARK")()
      _    <- MyServer.stop & Entry.nop
    } yield (res1, res2, res3)

    val \/-((res1, res2, res3)) = unsafeRun(task)

    res1 mustMatch {
      case net.Response(net.Code.Ok(_), _) => true
    }
    res2 mustMatch {
      case net.Response(net.Code.Ok(_), _) => true
    }
    res3 mustMatch {
      case net.Response(net.Code.Crash(_), _) => true
    }
  }
} //ServiceSpec
