lazy val prelude = RootProject(uri("https://gitlab.com/huli/prelude.git"))

lazy val util = (project in file("."))
  .dependsOn(prelude % "compile->compile;test->test")
  .settings(

  name         := "util",
  scalaVersion := "2.13.8",

  libraryDependencies ++= Seq(
    "org.bouncycastle" % "bcprov-jdk15on" % "1.70",
    "com.stripe"       % "stripe-java"    % "20.106.0",
  ),

  Compile / scalaSource       := baseDirectory.value / "src",
  Test    / scalaSource       := baseDirectory.value / "test",
  Compile / resourceDirectory := baseDirectory.value / "resources",

  console / initialCommands   += "import util._",

  assemblyExcludedJars in assembly := {
    (fullClasspath in assembly)
      .value
      .filter(_.data.getName == "KeyczarTool-0.71g-090613.jar")
  },

  scalacOptions ++= Seq(
    "-encoding", "utf8",      //
    "-explaintypes",          //better type error messages
    "-language:_",            //enable all language features
    "-opt:_",                 //enable all optimizations
    "-Yno-imports",           //do not auto-import scala, java.lang, Predef
    "-deprecation",           //enable deprecation warnings
    "-feature",               //enable feature warnings
    "-unchecked",             //enable unchecked warnings
  //"-opt-warnings:_",        //enable optimizer warnings
    "-Xfatal-warnings",       //warnings are errors
    "-Ywarn-dead-code",       //
    "-Ywarn-value-discard",   //
  ),

  Compile / console / scalacOptions --= Seq(
    "-opt:_",
    "-Yno-imports",
    "-Xfatal-warnings",
  ),
)

//======================================================================
scalapropsSettings
scalapropsVersion := "0.9.0"

// Build.sbt
