// https://postmarkapp.com/manual#implementing-email-sending-from-your-app
// https://postmarkapp.com/developer/user-guide/sending-email/sending-with-api
package util
package mail

import prelude._, std._
import data.{ Datum, DMap }
import net.{ Code, Response, Stringly }, net.dsl._, net.netty.{ Address, http }

import std.instances.data._
import data.instances.data._
import net.instances.data._

class Postmark(apiKey: String) extends Send {
  val api = new PostmarkAPI(apiKey)

  def send(mail: Mail): Task[Unit] =
    api.post(toDatum(mail)) flatMap { d =>
      unsafe.assert(d.map("ErrorCode").asLong == 0, "Error", d)
      val id = d.map("MessageID").asString
      Task0.unit & Log.info("postmark.id")(id) }

  def toDatum(mail: Mail) = DMap.empty +
    ("From"       -> mail.from)        +
    ("To"         -> mail.to)          +
    ("Bcc"        -> mail.bcc)         +
    ("Subject"    -> mail.subject)     +
    ("Tag"        -> mail.tag)         +
    ("TrackOpens" -> true)             +
    ("TrackLinks" -> "HtmlAndText")    +
    (mail.body match {
       case Body.Html(x) => "HtmlBody" -> x
       case Body.Text(x) => "TextBody" -> x } )
}

class PostmarkAPI(apiKey: String) extends net.Client(PostmarkEndpoint) {
  def POST = Datum ( Post upon Root / "email" ) header
             Stringly("X-Postmark-Server-Token", apiKey)

  def post(body: Datum) = POST(body) map Response.unsafeUnwrap[Datum, Datum]
}

object PostmarkEndpoint extends http.SecureClient(Address("api.postmarkapp.com", 443))

// Postmark
