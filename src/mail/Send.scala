// Transactional email.
package util
package mail

import prelude._, std._

trait Send {
  def send(mail: Mail): Task[Unit]
}

final case class Mail
  (from    : String,
   to      : String,
   bcc     : String,
   subject : String,
   tag     : String,
   body    : Body)

object Mail {
  def apply(from: String, to: String, subject: String, body: String): Mail =
    Mail(from, to, "", subject, "", Body.Text(body))
}

sealed trait Body extends Any
object Body {
  final case class Html(get: String) extends AnyVal with Body
  final case class Text(get: String) extends AnyVal with Body
}

// Send
