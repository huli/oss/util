package util
package mail
package instances

import prelude._, std._, data._
import typ.hlist._, typ.nat._
import std.instances.data._

trait DataInstances {

  implicit def DataMail: Data[Mail] = Struct(
    "Mail",
    "from"    -> empty[String] :+:
    "to"      -> empty[String] :+:
    "bcc"     -> empty[String] :+:
    "subject" -> empty[String] :+:
    "tag"     -> empty[String] :+:
    "body"    -> empty[Body]   :+:
    HNil )(
    (x: Mail) =>
      x.from :+: x.to :+: x.bcc :+: x.subject :+: x.tag :+: x.body :+: HNil )(
    (x: String:+:String:+:String:+:String:+:String:+:Body:+:HNil) =>
      Mail(x.i[_0], x.i[_1], x.i[_2], x.i[_3], x.i[_4], x.i[_5]) )

  implicit def DataBody: Data[Body] =
    Union[Body, Body.Html:+:Body.Text:+:HNil]("Body")

  implicit def DataHtml: Data[Body.Html] = Struct(
    "Html", "get" -> empty[String] :+: HNil )(
    (x: Body.Html) => x.get :+: HNil )(
    (x: String:+:HNil) => Body.Html(x.i[_0]) )

  implicit def DataText: Data[Body.Text] = Struct(
    "Text", "get" -> empty[String] :+: HNil )(
    (x: Body.Text) => x.get :+: HNil )(
    (x: String:+:HNil) => Body.Text(x.i[_0]) )

} //DataInstances
