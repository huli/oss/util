package util
package db

import prelude._, std._

import data.Data
import fsm.FID

sealed abstract class Blob[A: Data] {
  def delete: Blob[A] = this match {
    case obj: Blob.Obj[A]       => Blob.Tombstone(obj)
    case _  : Blob.Tombstone[A] => this }

  def getKey: FID = this match {
    case obj: Blob.Obj[A]       => obj.key
    case ts : Blob.Tombstone[A] => ts.get.key }

  def getValue: A = this match {
    case obj: Blob.Obj[A]       => obj.value
    case ts : Blob.Tombstone[A] => ts.get.value }

  def getOwner: FID = this match {
    case obj: Blob.Obj[A]       => obj.owner
    case ts : Blob.Tombstone[A] => ts.get.owner }

  def isTombstone: Boolean = this match {
    case _: Blob.Tombstone[A] => true
    case _                    => false }
}
object Blob {
  final case class Obj
    [A: Data]
    (key  : FID,
     value: A,
     label: String,
     tags : List[String],
     kind : String,
     owner: FID,
     ctime: Timestamp,
     mtime: Timestamp)
    extends Blob[A]
  {
    def update(partial: PartialObj[A]) = {
      val value = partial.value | this.value
      val label = partial.label | this.label
      val tags  = partial.tags  | this.tags
      copy(value=value, label=label, tags=tags, mtime=Timestamp.now)  }
  }

  final case class Tombstone[A: Data](get: Obj[A]) extends Blob[A]

  final case class PartialObj
    [A: Data]
    (value: Maybe[A]            = empty[A],
     label: Maybe[String]       = empty,
     tags : Maybe[List[String]] = empty)

} //Blob
