// TODO: alternative design: S3 key: owner/key -> prefix query
package util
package db

import prelude._, std._

import data.Data
import fsm.FID
import net.{ Request, Response }, net.dsl._, net.netty.crypto.Bits

import Blob._

import db.instances.data._
import fsm.instances.data.DataFID
import std.instances.data._

class Server
  [A: Data: ClassTag](name: String, blobs: FSM.Engine[A], context: Context)
  extends net.Server
{
  type Ref = FSM.Ref[A]

  val handle =
    Handle [PartialObj[A], Unit, Blob[A]] {
      case Post   upon Root / n      if n == name => post
      case Put    upon Root / n / id if n == name => put(id)    }|(

    Handle [Unit, Unit, Unit] {
      case Delete upon Root / n / id if n == name => delete(id) })|(

    Handle[Unit, Unit, Blob[A]] {
      case Get    upon Root / n / id if n == name => get(id)    })|(

    Handle [Unit, Unit, List[Blob[A]]] {
      case Get    upon Root / n      if n == name => multiget   })

//======================================================================
  def Handler
    [A: ClassTag: Data, B: Data]
    (handle: (Ref, A) => Task[Response[Unit, B]])
    : String => Request[A] => Task[Response[Unit, B]]
    = id => req => body(req) match {
      case Empty() =>
        Task0(Response.error[Unit, B](unit)) &
        Log.info("body")(unit)
      case Just(body) =>
        getOwner(req) flatMap {
          case Empty() =>
            Task0(Response.notFound[Unit, B](unit)) &
            Log.info("key")(unit)
          case Just(owner) =>
            checkOwner(owner, id) flatMap {
              case Empty() =>
                Task0(Response.notAuthorized[Unit, B](unit)) &
                Log.info("check")(unit)
              case Just(ref) =>
                handle(ref, body) } } }

  def getOwner(req: Request[_]): Task[Maybe[FID]] =
    getAuthHeader(req) match {
      case Empty()    => Task0(empty[FID]) & Entry.nop
      case Just(bits) => context.keyIdx.lookup(bits) }

  def checkOwner(owner: FID, blob: String): Task[Maybe[FSM.Ref[A]]] = {
    val id = Data[FID].fromDatum(data.DString(blob))
    context.blobIdx.lookup(owner) map {
      case Just(ids) if ids element id => blobs.unsafeCertify(id).just
      case _                           => empty } }

//======================================================================
  def post(req: Request[PartialObj[A]]): Task[Response[Unit, Blob[A]]] = for {
    owner  <- getOwner(req)
    partial = body(req)
    _      <- Task.fail("Server.post", "key")  if owner.isEmpty
    _      <- Task.fail("Server.post", "body") if partial.isEmpty
    body    = unsafe.unwrap(partial)
    _      <- Task.fail("Server.post", "value") if body.value.isEmpty
    obj     = Obj(FID(""),
                  unsafe.unwrap(body.value),
                  body.label | "",
                  body.tags  | nil,
                  name,
                  unsafe.unwrap(owner),
                  Timestamp.now,
                  Timestamp.now)
    ref    <- blobs.create(obj)
    s      <- ref ! Message.Initialize[A]()
  } yield Response.ok(s)

  val put = Handler[PartialObj[A], Blob[A]]((ref, partial) =>
    ref ! Message.Update(partial) map Response.ok[Unit, Blob[A]] )

  val delete = Handler[Unit, Unit]((ref, partial) =>
    ref ! Message.Delete[A]() map (_ => Response.ok[Unit, Unit](unit)) )

  val get = Handler[Unit, Blob[A]]((ref, partial) =>
    ref.? map (blob =>
      cond(blob.isTombstone,
           Response.notFound[Unit, Blob[A]](unit),
           Response.ok(blob) ) ) )

  def multiget(req: Request[Unit]) = for {
    owner <- getOwner(req)
    _     <- Task.fail("Server.multiget", "key") if owner.isEmpty
    mids  <- context.blobIdx.lookup(unsafe.unwrap(owner))
    ids    = mids getOrElse nil
    blobs <- parget(ids)
  } yield Response.ok[Unit, List[Blob[A]]](blobs)

  // FIXME: actually make this fetch in parallel...
  def parget(ids: List[FID]): Task[List[Blob[A]]] =
    (Task0(nil[Blob[A]]).&(Entry.nop) /: ids) { case task -> id =>
      task flatMap (res =>
        blobs.unsafeCertify(id).? map {
          case _: Tombstone[A] => res
          case obj             => obj::res } ) }

} //Server
