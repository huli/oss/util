package util
package db
package instances

import prelude._, std._

import data._
import fsm.FID
import net.netty.crypto.Bits
import typ.hlist._, typ.nat._

import data.instances.data._
import fsm.instances.data.DataFID
import std.instances.data._

trait DataInstances {

  // FIXME: move
  implicit def DataTriple[A: Data, B: Data, C: Data]: Data[(A, B, C)] =
    Data.instance(
      { case (a, b, c) => DList() :+ a :+ b :+ c },
      { case DList(a::b::c::Nil()) => (Data[A].fromDatum(a),
                                       Data[B].fromDatum(b),
                                       Data[C].fromDatum(c))
        case d => unsafe.abort("DataTriple", d) } )

  implicit val DataBits: Data[Bits] = Data.instance(
    x => DString(x.get),
    x => Bits.unsafeCertify(x.asString) )

//======================================================================
  import Blob._

  implicit def DataObj[A: Data]: Data[Obj[A]] = Struct(
    "Obj",
    "key"   -> empty[FID]          :+:
    "value" -> empty[A]            :+:
    "label" -> empty[String]       :+:
    "tags"  -> empty[List[String]] :+:
    "kind"  -> empty[String]       :+:
    "owner" -> empty[FID]          :+:
    "ctime" -> empty[Timestamp]    :+:
    "mtime" -> empty[Timestamp]    :+:
    HNil )(
    (x: Obj[A]) =>
    x.key:+:x.value:+:x.label:+:x.tags:+:x.kind:+:x.owner:+:x.ctime:+:x.mtime:+:HNil )(
    (x: FID:+:A:+:String:+:List[String]:+:String:+:FID:+:Timestamp:+:Timestamp:+:HNil) =>
    Obj(x.i[_0], x.i[_1], x.i[_2], x.i[_3], x.i[_4], x.i[_5], x.i[_6], x.i[_7]) )

  implicit def DataTombstone[A: Data]: Data[Tombstone[A]] = Struct(
    "Tombstone", "get" -> empty[Obj[A]] :+: HNil )(
    (x: Tombstone[A]) => x.get:+:HNil )(
    (x: Obj[A]:+:HNil) => Tombstone(x.i[_0]) )

  implicit def DataBlob[A: Data]: Data[Blob[A]] =
    Union[Blob[A], Obj[A]:+:Tombstone[A]:+:HNil]("Blob")

  // Custom, tagless repr to make external API more ergonomic.
  implicit def DataPartialObj[A: Data]: Data[PartialObj[A]] =
    Data.instance(partial =>
      DMap()
        .insert("value", partial.value)
        .insert("label", partial.label)
        .insert("tags",  partial.tags),

      d => {
        val m     = d.map
        val value = m.lookup("value").flatMap(Data[Maybe[A]].fromDatum)
        val label = m.lookup("label").flatMap(Data[Maybe[String]].fromDatum)
        val tags  = m.lookup("tags").flatMap(Data[Maybe[List[String]]].fromDatum)
        PartialObj(value, label, tags) } )

//======================================================================
  import Message._

  implicit def DataInitialize[A: Data]: Data[Initialize[A]] = Struct(
    "Initialize", HNil )(
    (x: Initialize[A]) => HNil )(
    (x: HNil)      => Initialize() )

  implicit def DataUpdate[A: Data]: Data[Update[A]] = Struct(
    "Update", "get" -> empty[PartialObj[A]] :+: HNil )(
    (x: Update[A])            => x.get:+:HNil )(
    (x: PartialObj[A]:+:HNil) => Update(x.i[_0]) )

  implicit def DataDelete[A: Data]: Data[Delete[A]] = Struct(
    "Delete", HNil )(
    (x: Delete[A]) => HNil )(
    (x: HNil)      => Delete() )

  implicit def DataMessage[A: Data]: Data[Message[A]] =
    Union[Message[A],
          Initialize[A]:+:
          Update[A]    :+:
          Delete[A]    :+:
          HNil]("db.Message")

//======================================================================
  import Action._

  implicit def DataIndex: Data[Index] = Struct(
    "Index",
    "key"   -> empty[FID] :+:
    "value" -> empty[FID] :+:
    HNil )(
    (x: Index)            => x.key:+:x.value:+:HNil )(
    (x: FID:+:FID:+:HNil) => Index(x.i[_0], x.i[_1]) )

  implicit def DataUnindex: Data[Unindex] = Struct(
    "Unindex",
    "key"   -> empty[FID] :+:
    "value" -> empty[FID] :+:
    HNil )(
    (x: Unindex)          => x.key:+:x.value:+:HNil )(
    (x: FID:+:FID:+:HNil) => Unindex(x.i[_0], x.i[_1]) )

  implicit def DataAction: Data[Action] =
    Union[Action, Index:+:Unindex:+:HNil]("db.Action")

} //DataInstances
