package util
package db

import prelude._, std._

import data.Data
import fsm.{ Description0, FID, Msg }

import db.instances.data._
import fsm.instances.data.DataFID
import std.instances.data._

import fsm.instances.ordered.OrderedFID

import Blob._

class FSM
  [A: Data](context: Context)
  extends Description0[Blob[A], Message[A], Action]
{
  final def initialState = unsafe.???

  final def transitions0(self: FID) = {
    case (obj: Obj[A]) -> Message.Initialize() =>
      obj.copy(key=self) -> List[Action](Action.Index(obj.owner, self))

    case (obj: Obj[A]) -> Message.Update(x) =>
      obj.update(x) -> nil

    case blob -> Message.Delete()    =>
      blob.delete -> List[Action](Action.Unindex(blob.getOwner, self)) }

  final def actions(self: FID) = {
    case Msg(Action.Index(owner, blob), _) =>
      context.blobIdx.transaction(owner) {
        case Empty() =>
          Task0(unit -> List(blob).just) &
          Log.info("indexed")(blob)
        case Just(blobs) =>
          val xs = Set.fromIList(blobs) union Set.fromIList(blob.list)
          Task0(unit -> xs.toIList.just) &
          Log.info("indexed")(blob) }

    case Msg(Action.Unindex(owner, blob), _) =>
      context.blobIdx.transaction(owner) {
        case Just(blobs) =>
          Task0(unit -> blobs.filterNot(_ == blob).just) &
          Log.info("unindexed")(blob)
        case empty =>
          Task0(unit -> empty) & Entry.nop } }
}

//======================================================================
sealed trait Message[A]
object Message {
  final case class Initialize[A: Data]()                   extends Message[A]
  final case class Update    [A: Data](get: PartialObj[A]) extends Message[A]
  final case class Delete    [A: Data]()                   extends Message[A]
}

sealed trait Action
object Action {
  final case class Index  (key: FID, value: FID) extends Action
  final case class Unindex(key: FID, value: FID) extends Action
}

//======================================================================
object FSM {
  type Ref[A] = fsm.FSMRef[Blob[A], Message[A], Action]
  type T[A]   = fsm.FSM[Blob[A], Message[A], Action]

  class Engine
    [A: Data]
    (name   : String,
     context: Context,
     store  : fsm.Store[Blob[A], Message[A], Action])
    extends fsm.Engine(
    name,
    new FSM[A](context),
    fsm.Policy.Default,
    store,
    fsm.Protocol.Sync[Blob[A], Message[A], Action] ) //!!!

} //FSM
