// https://ipstack.com/documentation
package util
package geo

import prelude._, std._
import data.Datum
import net.{ Code, Response, Stringly }, net.dsl._, net.netty.{ Address, http }

import data.instances.data._
import net.instances.data._

class Ipstack(apiKey: String) extends Lookup {
  val api = new IpstackAPI(apiKey)

  def lookup(ip: String): Task[IP] =  api.get(ip) map fromDatum

  private def fromDatum(d: Datum) = EXN.reraise("Ipstack.fromDatum", d) { IP(
    d.map("ip").asString,
    d.map("type").asString,
    d.map("country_code").asString,
    d.map("region_code").asString,
    d.map("city").asString,
    d.map("zip").asString,
    d.map("latitude").asDouble,
    d.map("longitude").asDouble,
    d.map("location").map("is_eu").asBoolean ) }
}

class IpstackAPI(apiKey: String) extends net.Client(IpstackEndpoint) {
  val ip = Symbol("ip")

  val GET = Datum ( Get upon Root / ip ) arg Stringly("access_key", apiKey)

  def get(ip: String) = GET(ip)() map Response.unsafeUnwrap[Datum, Datum]
}

object IpstackEndpoint extends http.Client(Address("api.ipstack.com", 80))

// Ipstack
