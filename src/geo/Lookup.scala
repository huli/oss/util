// Geo IP lookup.
package util
package geo

import prelude._, std._

trait Lookup {
  def lookup(ip: String): Task[IP]
}

final case class IP
  (ip        : String,
   version   : String,
   country   : String,
   region    : String,
   city      : String,
   zip       : String,
   latitude  : Double,
   longitude : Double,
   isEU      : Boolean)

// Lookup
