package util
package www

import prelude._, std._
import data.Charset.Utf8, data.format.Text
import net._, dsl._

import std.instances.data._

class Client(prim: net.PrimCall) extends net.Client(prim)
{
  override def formats = Text(Utf8) -> Text(Utf8)

  def get(path: String) =
    Call [String, String, String] (Get upon Path.fromString(path))

} //Client
