package util
package usr

import prelude._, std._
import fsm.FID
import net.netty.crypto.Bits

final case class Context
  [Entity, Settings]
  (basicRights: List[Entity],
   defaults   : Settings,
   init       : (FID, User[Entity, Settings]) => Task[Maybe[fsm.Msg[Message[Entity, Settings]]]],

   welcomeMail: (Address, Token) => mail.Mail,
   verifyMail : (Address, Token) => mail.Mail,
   resetMail  : (Address, Token) => mail.Mail,
   smtpd      : mail.Send,

   keyIdx     : io.Fragment[Bits,    FID], //key   -> user
   emailIdx   : io.Fragment[Address, FID], //email -> user

   cipher     : sec.Cipher,
   sessionTime: Duration = Days(31),
   verifyTime : Duration = Days(31),
   resetTime  : Duration = Hours(1))
{
  def SessionToken(id: FID) = Token(cipher)(id, Timestamp.now + sessionTime)
  def VerifyToken (id: FID) = Token(cipher)(id, Timestamp.now + verifyTime)
  def ResetToken  (id: FID) = Token(cipher)(id, Timestamp.now + resetTime)

  def parse(t: Token) = Token.parse(cipher)(t)

} //Context
