// TODO: Canceled -> Active??? / re-subbing in general
// TODO: check that metadata propagates
// TODO: verify webhook signatures
// TODO: poll stripe in case we have downtime / check for missed webhooks and alert
package util
package pymt

import prelude._, std._

import data.{ Data, Datum }
import fsm.FID
import net.{ Request, Response }, net.dsl._, net.netty.crypto.Bits

import std.instances.data._
import data.instances.data._
import net.instances.data._
import pymt.instances.data.DataSubscription

class Server
  [Entity: Data: ClassTag]
  (subscriptions: FSM.Engine[Entity], context: Context[Entity])
  extends net.Server
{
  type Ref = FSM.Ref[Entity]

  val handle =
    Handle [Body, Unit, Body] {
      case Post upon Root / "subscription" / "checkout" => checkout
      case Post upon Root / "subscription" / "portal"   => portal  }|(

    Handle [Datum, Unit, Unit] {
      case Post upon Root / "subscription" / "webhook"  => webhook })|(

    Handle [Unit, Unit, List[Subscription]] {
      case Get  upon Root / "subscription"              => list    })

//======================================================================
  def Handler
    [A: ClassTag: Data, B: Data]
    (handle: (Maybe[FID], A) => Task[B])
    : Request[A] => Task[Response[Unit, B]]
    = req => (getAuthHeader(req), body(req)) match {
      case (Just(bits), Just(a)) =>
        context.keyIdx.lookup(bits) flatMap
        (handle(_, a))              map
        Response.ok[Unit, B]
      case _ =>
        Task0(Response.error[Unit, B](unit)) &
        Log.warn("subscription.request")(req) }

  def Hook
    [A: ClassTag: Data, B: Data]
    (handle: A => Task[B])
    : Request[A] => Task[Response[Unit, B]]
    = req => body(req) match {
      case Just(a) => handle(a) map Response.ok[Unit, B]
      case _       =>
        Task0(Response.error[Unit, B](unit)) &
        Log.warn("subscription.request")(req) }

//======================================================================
  val checkout = Handler[Body, Body](
    (mfid, body) => for {
      _   <- Task.fail("Server.checkout", "key") if mfid.isEmpty
      fid  = unsafe.unwrap(mfid)
      ref  = context.users.unsafeCertify(fid)
      usr <- ref.?
      id  <- context.stripe.createCheckoutSession(
        fid.get,
        usr.email.address.get,
        body.lookup("price")       |> unsafe.unwrap,
       (body.lookup("trial_days")  |> unsafe.unwrap).toLong,
        body.lookup("success_url") |> unsafe.unwrap,
        body.lookup("failure_url") |> unsafe.unwrap ) & Entry.nop
    } yield Map("session_id" -> id) )

  val portal = Handler[Body, Body](
    (mfid, body) => for {
      _    <- Task.fail("Server.portal", "key") if mfid.isEmpty
      fid   = unsafe.unwrap(mfid)
      msid <- context.subscriptionIdx.lookup(fid).map(_ flatMap (_.headMaybe))
      _    <- Task.fail("Server.portal", "sid") if msid.isEmpty
      sid   = unsafe.unwrap(msid)
      sub  <- subscriptions.unsafeCertify(sid).?
      url  <- context.stripe.createPortalSession(
        sub.customer,
        body.lookup("return_url") |> unsafe.unwrap ) & Entry.nop
    } yield Map("url" -> url) )

  val list = Handler[Unit, List[Subscription]](
    (mfid, _) => for {
      _     <- Task.fail("Server.portal", "key") if mfid.isEmpty
      fid    = unsafe.unwrap(mfid)
      msids <- context.subscriptionIdx.lookup(fid)
      sids   = msids | nil
      subs  <- parget(sids)
    } yield subs )

  // FIXME: actually make this fetch in parallel...
  private def parget(ids: List[FID]): Task[List[Subscription]] =
    (Task0(nil[Subscription]).&(Entry.nop) /: ids) { case task -> id =>
      task flatMap (res =>
        subscriptions.unsafeCertify(id).? map (_::res) ) }

  val webhook = Hook[Datum, Unit] { body =>
    Task0(body) & Log.info("stripe.event")(body) flatMap0
    context.stripe.parseEvent                    flatMap {
      case Empty() =>
        Task0(unit) ~> Log.warn("stripe.event.nyi")
      case Just((uid_, eid, sub)) =>
        val uid = FID(uid_)
        context.users.unsafeCertify(uid).? flatMap { usr =>
          context.subscriptionIdx.lookup(uid) flatMap {
            case Empty() =>
              subscriptions.create flatMap
              (_ send Message.Create(uid, usr.email.address, eid, sub))
            case Just(sub0::Nil()) =>
              val ref = subscriptions.unsafeCertify(sub0)
              // TODO: check that subscription IDs match?
              ref send Message.Update(uid, usr.email.address, eid, sub)

            case _ => unsafe.abort("Server.webhook", "nyi")
          } } } }

} //Server
