package util
package pymt

package util
package usr

import prelude._, std._
import data.Data
import net._, dsl._, netty.crypto.Bits

import std.instances.data._

class Client
  [Entity: Data: Ordered, Settings: Data](prim: PrimCall)
    extends net.Client(prim)
{
  def checkout(key: Bits) =
    Call [Body, Unit, Body] (Post upon Root / "subscription" / "checkout")
      .header(AuthHeader(key))

  def portal(key: Bits) =
    Call [Body, Unit, Body] (Post upon Root / "subscription" / "portal")
      .header(AuthHeader(key))

} //Client
