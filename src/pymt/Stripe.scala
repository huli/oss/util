// TODO: use HTTP api directly!
package util
package pymt

import prelude._, std._
import data.Datum

import java.lang.Object
import java.util.{ ArrayList, HashMap }
import com.stripe.model
import com.stripe.model.billingportal.{ Session => Portal }
import com.stripe.model.checkout.{ Session => Checkout }
import com.stripe.net.RequestOptions
import com.stripe.param

import data.instances.data._

import Stripe._

trait Stripe {
  def createCheckoutSession
    (user     : ID,     // \ backend
     email    : String, // /
     price    : ID,     // external
     trialDays: Long,   //
     succ     : URL,    // \ frontend
     fail     : URL)    // /
    : Task0[ID]         //external

  def createPortalSession
    (customer: ID,     //external
     ret     : URL)    //frontend
    : Task0[URL]       //external

  def parseEvent
    (json: Datum)
    : Task0[Maybe[
    (ID,               //backend (user)
     ID,               //external (event)
     Subscription)]]
}
object Stripe {
  type ID  = String
  type URL = String
}

//======================================================================
class SDK(apiKey: String) extends Stripe
{
  def createCheckoutSession
    (user: ID, email: String, price: ID, trialDays: Long, succ: URL, fail: URL)
    = {
    val paymentMethodTypes = new ArrayList[String]
    paymentMethodTypes.add("card")

    val lineItems = new ArrayList[Object]
    val lineItem1 = new HashMap[String, Object]
    lineItem1.put("price",    price)
    lineItem1.put("quantity", 1.asInstanceOf[Object])
    lineItems.add(lineItem1)

    val metadata = new HashMap[String, Object]
    metadata.put("uid", user)

    val subscriptionData = new HashMap[String, Object]
    subscriptionData.put("metadata",          metadata)
    subscriptionData.put("trial_period_days", trialDays.asInstanceOf[Object])

    val params = new HashMap[String, Object]
    params.put("cancel_url",           fail)
    params.put("mode",                 "subscription")
    params.put("payment_method_types", paymentMethodTypes)
    params.put("success_url",          succ)
    params.put("customer_email",       email)
    params.put("line_items",           lineItems)
    params.put("metadata",             metadata)
    params.put("subscription_data",    subscriptionData)

    val requestOptions = (new RequestOptions.RequestOptionsBuilder)
      .setApiKey(apiKey)
      .build

    Task0 { Checkout.create(params, requestOptions).getId }
  }

//----------------------------------------------------------------------
  def createPortalSession
    (customer: ID, ret: URL)
    = {
    val params = new HashMap[String, Object]
    params.put("customer",   customer)
    params.put("return_url", ret)

    val requestOptions = (new RequestOptions.RequestOptionsBuilder)
      .setApiKey(apiKey)
      .build

    Task0 { Portal.create(params, requestOptions).getUrl }
  }

//----------------------------------------------------------------------
  // TODO: add mail to metadata for easier eyeballing of events?
  def retrieveSubscription(id: String): Task0[Datum] = {
    //val params = (new com.stripe.param.EventRetrieveParams.Builder)
    //  .addExpand("")
    //  .build
    val options = (new RequestOptions.RequestOptionsBuilder)
      .setApiKey(apiKey)
      .build
    Task0(com.stripe.model.Subscription.retrieve(id, options))
      .map0(_.toJson)
      .map0(data.Data.fromJson[Datum]) }

  def parseEvent(event: Datum): Task0[Maybe[(ID, ID, Subscription)]] = {

    def doParseEvent(d: Datum): Maybe[(ID, ID, Status)] = {
      val eid   = d.map("id").asString
      val obj   = d.map("data").map("object").map
      val maybe = d.map("type").asString match {
        case "checkout.session.completed" =>
          (obj("subscription").asString -> Trialing).just
        case "customer.subscription.trial_will_end" =>
          (obj("id").asString -> Active).just
        // NOP invoice created when subs with a free trial are started, ignore.
        case "invoice.paid"
            if obj("billing_reason").asString == "subscription_create" &&
               obj("amount_due").asLong       == 0 =>
          empty
        case "invoice.paid" =>
          (obj("subscription").asString -> Active).just
        case "invoice.payment_failed" =>
          (obj("subscription").asString -> PastDue).just
        case "customer.subscription.deleted" =>
          (obj("id").asString -> Canceled).just
        case _ => empty }
      maybe map { case sid -> status => (eid, sid, status) } }

    def parseSubscription(d: Datum): (ID, ID, ID, Long) =
      (d.map("customer").asString,
       d.map("metadata").map("uid").asString,
       d.map("items").map("data").list(0).map("price").map("id").asString,
       d.map("current_period_end").asLong)

    Task0(doParseEvent(event)) flatMap0 {
      case Just((eid, sid, status)) =>
        retrieveSubscription(sid) map0 { d =>
          val (cid, uid, price, end) = parseSubscription(d)
          (uid, eid, Subscription(status, sid, cid, price, end)).just }
      case Empty() => Task0(empty[(ID, ID, Subscription)]) }
  }

}

// Stripe
