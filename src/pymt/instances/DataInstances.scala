package util
package pymt
package instances

import prelude._, std._, data._
import fsm.FID
import typ.hlist._, typ.nat._

import usr.Address

import std.instances.data._
import data.instances.data._
import fsm.instances.data._

import usr.instances.data.DataAddress
import mail.instances.data.DataMail

trait DataInstances {

  implicit def DataSubscription
    : Data[Subscription] = Struct(
    "Subscription",
    "status"   -> empty[Status] :+:
    "id"       -> empty[String] :+:
    "customer" -> empty[String] :+:
    "price"    -> empty[String] :+:
    "end"      -> empty[Long]   :+:
    HNil )(
    (x: Subscription) =>
    x.status:+:x.id:+:x.customer:+:x.price:+:x.end:+:HNil )(
    (x: Status:+:String:+:String:+:String:+:Long:+:HNil) =>
    Subscription(x.i[_0], x.i[_1], x.i[_2], x.i[_3], x.i[_4]) )

  implicit val DataStatus: Data[Status] =
    Enum[Status](
      "Subscription.Status",
      List(Trialing, Active, PastDue, Canceled),
      _.toLowerCase)

//======================================================================
  import Message._

  implicit def DataCreate: Data[Create] = Struct(
    "Create",
    "uid"          -> empty[FID]     :+:
    "address"      -> empty[Address] :+:
    "eventID"      -> empty[String]  :+:
    "subscription" -> empty[Subscription] :+:
    HNil )(
    (x: Create) => x.uid:+:x.address:+:x.eventID:+:x.subscription:+:HNil )(
    (x: FID:+:Address:+:String:+:Subscription:+:HNil) =>
    Create(x.i[_0], x.i[_1], x.i[_2], x.i[_3] ) )

  implicit def DataUpdate: Data[Update] = Struct(
    "Update",
    "uid"          -> empty[FID]     :+:
    "address"      -> empty[Address] :+:
    "eventID"      -> empty[String]  :+:
    "subscription" -> empty[Subscription] :+:
    HNil )(
    (x: Update) => x.uid:+:x.address:+:x.eventID:+:x.subscription:+:HNil )(
    (x: FID:+:Address:+:String:+:Subscription:+:HNil) =>
    Update(x.i[_0], x.i[_1], x.i[_2], x.i[_3] ) )

  implicit def DataMessage: Data[Message] =
    Union[Message,
          Create :+:
          Update :+:
          HNil]("Message")

//======================================================================
  import Action._

  implicit def DataIndexSub[E]: Data[IndexSub[E]] = Struct(
    "IndexSub",
    "key"   -> empty[FID] :+:
    "value" -> empty[FID]     :+:
    HNil )(
    (x: IndexSub[E]) => x.key:+:x.value:+:HNil )(
    (x: FID:+:FID:+:HNil) => IndexSub(x.i[_0], x.i[_1]) )

  implicit def DataSend[E]: Data[Send[E]] = Struct(
    "Send",
    "get" -> empty[mail.Mail] :+:
    HNil )(
    (x: Send[E]) => x.get:+:HNil )(
    (x: mail.Mail:+:HNil) => Send(x.i[_0]) )

  implicit def DataPutRights[E: Data: Ordered]: Data[PutRights[E]] = Struct(
    "PutRights",
    "uid" -> empty[FID]     :+:
    "get" -> empty[List[E]] :+:
    HNil )(
    (x: PutRights[E]) => x.uid:+:x.get:+:HNil )(
    (x: FID:+:List[E]:+:HNil) => PutRights(x.i[_0], x.i[_1] ) )

  implicit def DataDeleteRights
    [E: Data: Ordered]: Data[DeleteRights[E]] = Struct(
    "DeleteRights",
    "uid" -> empty[FID]     :+:
    "get" -> empty[List[E]] :+:
     HNil )(
    (x: DeleteRights[E]) => x.uid:+:x.get:+:HNil )(
    (x: FID:+:List[E]:+:HNil)  => DeleteRights(x.i[_0], x.i[_1] ) )

  implicit def DataAction[E: Data: Ordered]: Data[Action[E]] =
    Union[Action[E],
          IndexSub[E]    :+:
          Send[E]        :+:
          PutRights[E]   :+:
          DeleteRights[E]:+:
          HNil]("Message")

} //DataInstances
