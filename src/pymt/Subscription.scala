package util
package pymt

import prelude._, std._

final case class Subscription
  (status  : Status,
   id      : String, //Stripe subscription ID
   customer: String, //Stripe user ID
   price   : String, //Stripe product ID
   end     : Long)   //UNIX timestamp

sealed trait Status
final case object Trialing extends Status
final case object Active   extends Status
final case object PastDue  extends Status
final case object Canceled extends Status

// Subscription
