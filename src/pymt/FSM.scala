package util
package pymt

import prelude._, std._
import data.Data
import fsm.{ Description0, FID, Msg }

import usr.Address

import fsm.instances.data.DataFID

import pymt.instances.data._

import Message._, Action._

class FSM
  [Entity: Data: Ordered]
  (context: Context[Entity])
  extends Description0[Subscription, Message, Action[Entity]]
{
  def initialState = FSM.s0

  def transitions0(self: FID) = {
    case _s0 -> Create(uid, address, _, s1) =>
      unsafe.assert(s1.status == Trialing, "Subscription.Create", s1)
      val entities = context.plans.lookup(s1.price) |> unsafe.unwrap
      s1 -> List[Action[Entity]](
        IndexSub[Entity](uid, self),
        PutRights[Entity](uid, entities),
        Send[Entity](context.trialStartMail(address)) )

    case Subscription(Trialing, _, _, _, _) ->
         Update(_, address, _, s @ Subscription(Active, _, _, _, _)) =>
      s -> List[Action[Entity]](
        Send[Entity](context.trialEndMail(address)) )

    case Subscription(Active, _, _, _, _) ->
         Update(_, _, _, s @ Subscription(Active, _, _, _, _)) =>
      s -> nil[Action[Entity]]

    case Subscription(Active, _, _, _, _) ->
         Update(_, address, _, s @ Subscription(PastDue, _, _, _, _)) =>
      s -> List[Action[Entity]](
        Send[Entity](context.paymentFailedMail(address)) )

    case Subscription(PastDue, _, _, _, _) ->
         Update(_, _, _, s @ Subscription(Active, _, _, _, _)) =>
      s -> nil[Action[Entity]]

      // FIXME: PastDue -> Canceled automatically?
      // TODO: PastDue + PastDue = Canceled?
      // TODO: DeleteRights(atPeriodEnd)
      //       * send when entering pastdue and canceled
      //       * re-send putrights when entering active from pastdue
      //       * putrights revokes previously pending deleterights

    case Subscription(_, _, _, _, _) ->
         Update(uid, address, _, s @ Subscription(Canceled, _, _, _, _)) =>
      val entities = context.plans.lookup(s.price) |> unsafe.unwrap
      s -> List[Action[Entity]](
        Send[Entity](context.subscriptionCanceledMail(address)),
        DeleteRights[Entity](uid, entities) )
  }

//======================================================================
  def actions(self: FID) = {
    case Msg(Send(mail), _) =>
      context.smtpd send mail
    case Msg(IndexSub(uid, sid), _) =>
      context.subscriptionIdx.transaction(uid) {
        case Empty() =>
          Task0(unit -> List(sid).just) &
          Log.info("indexed")(sid)
        case Just(sids) =>
          Task0(unit -> (sid::sids).just) &
          Log.info("indexed")(sid) }
    case Msg(PutRights(uid, entities), _) =>
      context.users.unsafeCertify(uid) send usr.Message.putRights(entities)
    case Msg(DeleteRights(uid, entities), _) =>
      context.users.unsafeCertify(uid) send usr.Message.deleteRights(entities)
  }
}

//======================================================================
object FSM {
  def s0 = Subscription(Canceled, "", "", "", 0)

  type Ref[E] = fsm.FSMRef[Subscription, Message, Action[E]]

  type T[E]   = fsm.FSM[Subscription, Message, Action[E]]

  class Engine
    [E: Data: Ordered]
    (context: Context[E],
     store  : fsm.Store[Subscription, Message, Action[E]])
    extends fsm.Engine(
    "subscriptions",
    new FSM[E](context),
    fsm.Policy.Default,
    store,
    fsm.Protocol.Sync[Subscription, Message, Action[E]] ) //!!!
}

//======================================================================
sealed trait Message
object Message {
  final case class Create
    (uid         : FID,
     address     : Address,
     eventID     : String, //for logging only
     subscription: Subscription)
    extends Message

  final case class Update
    (uid         : FID,
     address     : Address,
     eventID     : String,
     subscription: Subscription)
    extends Message
}

//======================================================================
sealed trait Action[E]
object Action {
  final case class Send        [E](get: mail.Mail)         extends Action[E]
  final case class IndexSub    [E](key: FID, value: FID)   extends Action[E]
  final case class PutRights   [E](uid: FID, get: List[E]) extends Action[E]
  final case class DeleteRights[E](uid: FID, get: List[E]) extends Action[E]
}

// FSM
