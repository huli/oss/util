package util

import prelude._, std._, data._

package object sec {
  type Encrypted[A] = Encrypted.T[A]

  implicit def DataEncrypted[A: Data]: Data[Encrypted[A]] = Encrypted.DataEncrypted[A]

} //package
