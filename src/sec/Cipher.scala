package util
package sec

import javax.crypto.{ Cipher => JXCipher, SecretKeyFactory }
import javax.crypto.spec.{ PBEKeySpec, SecretKeySpec }

import prelude._, std._
import data.Charset.{ Latin1, Utf8 }, net.netty.crypto
import Cipher._

trait Cipher {
  val encrypt: Plaintext  => Ciphertext
  val decrypt: Ciphertext => Plaintext
}
object Cipher {
  type Plaintext  = String
  type Ciphertext = String
}

class AES(key: String, salt: String) extends Cipher
{
  val encrypt = Latin1.bytes(_) |> encrypt_ |> crypto.hex
  val decrypt = _ |> crypto.unHex |> decrypt_ |> Utf8.string

  def encrypt_(x: Bytes): Bytes = {
    val cipher = JXCipher.getInstance(ALGO)
    cipher.init(JXCipher.ENCRYPT_MODE, keySpec)
    cipher.doFinal(x) }

  def decrypt_(x: Bytes): Bytes = {
    val cipher = JXCipher.getInstance(ALGO)
    cipher.init(JXCipher.DECRYPT_MODE, keySpec)
    cipher.doFinal(x) }

  def keySpec: SecretKeySpec = {
    val key_   = key.toCharArray
    val salt_  = Latin1.bytes(salt)
    val skf    = SecretKeyFactory.getInstance(HASHING_ALGO)
    val pks    = new PBEKeySpec(key_, salt_, ITERATION_COUNT, KEY_LENGTH)
    val secret = skf.generateSecret(pks).getEncoded
    new SecretKeySpec(secret, "AES") }

  def ALGO          = "AES/ECB/PKCS5Padding"
  def HASHING_ALGO  = "PBKDF2WithHmacSHA512"
  def ITERATION_COUNT = 999999
  def KEY_LENGTH      = 128
}

// Cipher
