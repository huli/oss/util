// API keys.
package util
package sec

import prelude._, std._
import net.netty.crypto.Bits
import svc.Service

final case class Key
  [Entity]
  (secret: Bits         = Bits(),
   mode  : Service.Mode = Service.Mode.Test,
   status: Status       = Status.Passive,
   rights: ACL[Entity])
{
  def active = status == Status.Active
  def live   = mode   == Service.Mode.Live
}
object Key {
  def apply[Entity: Ordered](): Key[Entity] = Key(rights = ACL())

  // FIXME: header safe...
  def unpadded[Entity: Ordered](): Key[Entity] =
    Bits() match {
      case bits if bits.get endsWith "==" => unpadded()
      case bits                           => Key(secret=bits, rights = ACL()) }
}

sealed trait Status
object Status {
  case object Active  extends Status
  case object Passive extends Status
}

// Key
