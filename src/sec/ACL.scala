// Access control lists: can be stored either with the user (and contain
// a object -> permissions mapping) or the object (and contain a user ->
// permissions mapping, i.e. clists); default is the former (c.f. Key).
package util
package sec

import prelude._, std._
import Permission._

final case class ACL[A: Ordered](map: Map[A, Mode] = Map.empty[A, Mode]) {
  def get(a: A): Maybe[Mode] =
    map lookup a
  def get(a: A, p: Permission): Boolean =
    map lookup a map (_ get p) getOrElse false

  def put(a: A, m: Mode): ACL[A] =
    copy(map insert (a, m))
  def put(a: A, p: Permission): ACL[A] =
    copy(map alter (a, _ getOrElse Mode() put p just))

  def delete(a: A): ACL[A] =
    copy(map delete a)
  def delete(a: A, p: Permission): ACL[A] =
    copy(map adjust (a, _ delete p))
}

final case class Mode
  (read   : Boolean = false,
   write  : Boolean = false,
   execute: Boolean = false)
{
  val get: Permission => Boolean = {
    case Read    => read
    case Write   => write
    case Execute => execute
  }
  val put: Permission => Mode = {
    case Read    => copy(read    = true)
    case Write   => copy(write   = true)
    case Execute => copy(execute = true)
  }
  val delete: Permission => Mode = {
    case Read    => copy(read    = false)
    case Write   => copy(write   = false)
    case Execute => copy(execute = false)
  }
}

sealed trait Permission
object Permission {
  case object Read    extends Permission
  case object Write   extends Permission
  case object Execute extends Permission
}

// ACL
